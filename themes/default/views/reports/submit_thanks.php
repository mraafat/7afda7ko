<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <title>Animated Background Headers | Demo 3</title>
        <meta name="description" content="Examples for creative website header animations using Canvas and JavaScript" />
        <meta name="keywords" content="header, canvas, animated, creative, inspiration, javascript" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" type="text/css" href="../themes/default/css/thanks_normalize.css" />
        <link rel="stylesheet" type="text/css" href="../themes/default/css/thanks_demo.css" />
        <link rel="stylesheet" type="text/css" href="../themes/default/css/thanks_component.css" />
        <link href='http://fonts.googleapis.com/css?family=Raleway:200,400,800|Londrina+Outline' rel='stylesheet' type='text/css'>
        <!--[if IE]>
        	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
	</head>
	<body>
		<div class="container demo-3">
			<div class="content">
                <div id="large-header" class="large-header">
                    <canvas id="demo-canvas"></canvas>
                    <h1 class="main-title">شكرآ لأستخدامك حفضحكوا، لما حد يقرب من مكان شكوتك حبعتله علي طول</span></h1>
                </div>
        </div>
		</div><!-- /container -->
        <script src="../themes/default/js/TweenLite.min.js"></script>
        <script src="../themes/default/js/EasePack.min.js"></script>
        <script src="../themes/default/js/rAF.js"></script>
        <script src="../themes/default/js/demo-3.js"></script>
	</body>
</html>