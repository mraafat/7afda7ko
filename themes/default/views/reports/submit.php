<html lang="en" class="no-js" dir="rtl">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>7afda7ko</title>
	<meta name="description" content="Fullscreen Form Interface: A distraction-free form concept with fancy animations" />
	<meta name="keywords" content="fullscreen form, css animations, distraction-free, web design" />
	<meta name="author" content="Codrops" />
	<link rel="shortcut icon" href="../favicon.ico">
	
	<link rel="stylesheet" type="text/css" href="../themes/default/css/font.css" />
	<link rel="stylesheet" type="text/css" href="../themes/default/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="../themes/default/css/demo.css" />
	<link rel="stylesheet" type="text/css" href="../themes/default/css/component.css" />
	<link rel="stylesheet" type="text/css" href="../themes/default/css/cs-select.css" />
	<link rel="stylesheet" type="text/css" href="../themes/default/css/cs-skin-boxes.css" />
	<style>
	#map-canvas {
		height: 300px;
		margin: 0px;
		padding: 0px;
		width: 100%;
		border-radius: 20px;
	}
	</style>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
	<script src="../themes/default/js/modernizr.custom.js"></script>
	<script src="../themes/default/js/googlemaps.js"></script>
	
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-57280686-1', 'auto');
	ga('send', 'pageview');

	</script>

</head>
<body>
	<div class="container">

		<div class="fs-form-wrap" id="fs-form-wrap">
			<div class="fs-title" >
				<h1 style = "color:white !important;color: white !important;float: left; font-family: 'JF Flat Regular',Sans-Serif;">أملي الفورمه</h1>
			</div>
			<!-- red-box -->
			<div class="red-box">
				<ul>
					<?php
					foreach ($errors as $error_item => $error_description)
					{
						print (!$error_description) ? '' : "<li>" . $error_description . "</li>";
					}
					?>
				</ul>
			</div>
			<?php print form::open(NULL, array('enctype' => 'multipart/form-data', 'id' => 'myform', 'name' => 'reportForm', 'class' => 'fs-form fs-form-full' , 'autocomplete'=>'off')); ?>
			<!-- <form id="myform" class="fs-form fs-form-full" autocomplete="off"> -->
			<input type="hidden" name="latitude"  id="latitude" value="<?php echo $form['latitude']; ?>">
			<input type="hidden" name="longitude" id="longitude" value="<?php echo $form['longitude']; ?>">
			<input type="hidden" name="country_name" id="country_name" value="<?php echo $form['country_name']; ?>" />
			<input type="hidden" name="incident_zoom" id="incident_zoom" value="<?php echo $form['incident_zoom']; ?>" />
			<div style="display:none;">
				<input type="hidden" name="incident_date" id="" value="<?php echo $form['incident_date']; ?>" />
			</div>
			<input type="hidden" name="incident_hour" id="incident_hour" value="<?php echo $form['incident_hour']; ?>" />
			<input type="hidden" name="incident_minute" id="incident_minute" value="<?php echo $form['incident_minute']; ?>" />
			<input type="hidden" name="incident_ampm" id="incident_zoom" value="<?php echo $form['incident_ampm']; ?>" />
			
			<ol class="fs-fields">

				<li>
					<label class="fs-field-label fs-anim-upper" for="q1">أسمك أيه؟</label>
					<!--<input class="fs-anim-lower" id="q1" name="q1" type="text" placeholder="ekteb hena" required/>-->
					<?php print form::input('person_first', $form['person_first'], 'class="fs-anim-lower" id="q1" name="q1" type="text" placeholder="أكتب هنا" required'); ?>

				</li>
				
				
				<li>
					<label class="fs-field-label fs-anim-upper" for="q2" data-info="matkhafsh, mesh 7anspamak">أيه الميل بتاعك?</label>
					<!-- <input class="fs-anim-lower" id="q2" name="q2" type="email" placeholder="3abdelsamee3lamee3@gmail.com" required/> -->
					<?php print form::input('person_email', $form['person_email'], ' class="fs-anim-lower" type="email" placeholder="3abdelsamee3lamee3@gmail.com" required'); ?>

				</li>
				<li>													
					<label class="fs-field-label fs-anim-upper" for="q5">مين اللي زعلك؟</label>							

					<?php print form::input('location_name', $form['location_name'], ' class="fs-anim-lower" id="q5" placeholder="أسم المحل/مدرسه/شركه.."'); ?>
				</li>
				<li>
					<label class="fs-field-label fs-anim-upper" for="q3" data-info="matkhafsh, mesh 7anspamak">أكتب اللي حصل في كلمتين </label><p class = "fs-anim-upper myhint"> متخفش السؤال الي جي حتشرح بالتفصيل</p>
					<?php print form::input('incident_title', $form['incident_title'], ' class="text long fs-anim-lower"'); ?>
				</li>
				<li>
					<label class="fs-field-label fs-anim-upper" for="q3">أيه اللي حصل بالتفصيل الممل</label>
					<?php print form::textarea('incident_description', $form['incident_description'], ' rows="10" class="fs-anim-lower" id="q3" placeholder="بالراحه و من غير أي الفاظ خارجه"') ?>
					
				</li>
				
				
				
				<li>
					<label class="fs-field-label fs-anim-upper" for="q4">فين المكان اللي حصل فيه الموقف؟ </label><p class = "fs-anim-upper map_hint"> أمسك السهم الأحمر و حطه بالظبط في المكان اللي حصلك فيه المشكله</p>	
							
					<div id="map-canvas" for="q4" dir="ltr" class="fs-anim-lower"></div>
				</li>
				
				
				
				
				
				
				
				
				
				<!--<li id="locationpage" >													
					<label class="fs-field-label fs-anim-upper" for="q4">فين المكان اللي حصل فيه الموقف؟ </label><p class = "fs-anim-upper map_hint"> أمسك السهم الأحمر و حطه بالظبط في المكان اللي حصلك فيه المشكله</p>							
					<div id="divMap" class="report_map fs-anim-lower" >
					</div>	
					<?php print form::input('location_find', '', ' title="'.Kohana::lang('ui_main.location_example').'" class="findtext search_map_field fs-anim-upper" placeholder="العنوان"'); ?>
						<input type="button" name="button" id="button" value="دور علي المكان ده"  class="btn_find search_map_btn fs-anim-upper" />
						<div id="find_loading" class="report-find-loading"></div>
						

				</li>
			-->


			<li>
				<label class="fs-field-label fs-anim-upper" for="q5">نوع الشكوي</label>							
				<div class="report_category fs-anim-lower" id="categories">
					<?php
					$selected_categories = (!empty($form['incident_category']) AND is_array($form['incident_category']))
					? $selected_categories = $form['incident_category']
					: array();


					echo category::form_tree('incident_category', $selected_categories, 2);
					?>
				</div>
			</li>

						<!--<li>
							<label class="fs-field-label fs-anim-upper" for="q5">Feen el makan eli 7asal feeh el moshkela</label>
							<input class="fs-mark fs-anim-lower" id="q5" name="q5" type="number" placeholder="1000" step="100" min="100"/>
						</li> -->
					</ol><!-- /fs-fields -->
					<button class="fs-submit" type="submit">شكرآ، متقلقش حنطييير بشكوتك</button>
					
					
					
				</form><!-- /fs-form -->
			</div><!-- /fs-form-wrap -->


		</div><!-- /container -->
		<a id="exit" class="fs-field-label fs-anim-upper notnow " href="http://www.7afda7ko.com/closeInAppBrowser.html">مش هبلغ دلوقتي</a>

		<script src="../themes/default/js/classie.js"></script>
		<script src="../themes/default/js/selectFx.js"></script>
		<script src="../themes/default/js/fullscreenForm.js"></script>
		<script>
		(function() {
			
			var formWrap = document.getElementById( 'fs-form-wrap' );

			[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
				new SelectFx( el, {
					stickyPlaceholder: false,
					onChange: function(val){
						document.querySelector('span.cs-placeholder').style.backgroundColor = val;
					}
				});
			} );

			new FForm( formWrap, {
				onReview : function() {
						classie.add( document.body, 'overview' ); // for demo purposes only
					}
				} );
		})();
		</script>
		<script>
		$( document ).ready(function() {
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
//Write mobile code here

}

});
		</script>
	<script>	
	function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
var type = getParameterByName('type');
if(type!="app")
{
	$("#exit").css("display","none");
}
else{
	$("#exit").css("display","block");
}
</script>
	</body>
	</html>